<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route untuk menampilkan halaman login
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');

// Route untuk melakukan proses login
Route::post('/login', 'Auth\LoginController@login');

// Route untuk menampilkan halaman register
Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');

// Route untuk melakukan proses register
Route::post('/register', 'Auth\RegisterController@register');

// Route untuk melakukan proses logout
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
