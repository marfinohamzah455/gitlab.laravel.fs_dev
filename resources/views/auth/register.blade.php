@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" id="register-form">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function () {
        // Register form submission
        $('#register-form').submit(function (event) {
            event.preventDefault();

            var form = $(this);
            var url = form.attr('action');
            var data = form.serialize();

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (response) {
                    window.location.href = "{{ route('home') }}";
                },
                error: function (xhr, status, error) {
                    var err = JSON.parse(xhr.responseText);
                    var errors = err.errors;

                    // Display errors on form
                    if (errors.hasOwnProperty('name')) {
                    $('#name').addClass('is-invalid');
                    $('#name-error').text(errors['name'][0]);
                } else {
                    $('#name').removeClass('is-invalid');
                    $('#name-error').text('');
                }

                if (errors.hasOwnProperty('email')) {
                    $('#email').addClass('is-invalid');
                    $('#email-error').text(errors['email'][0]);
                } else {
                    $('#email').removeClass('is-invalid');
                    $('#email-error').text('');
                }

                if (errors.hasOwnProperty('password')) {
                    $('#password').addClass('is-invalid');
                    $('#password-error').text(errors['password'][0]);
                } else {
                    $('#password').removeClass('is-invalid');
                    $('#password-error').text('');
                }

                if (errors.hasOwnProperty('password_confirmation')) {
                    $('#password-confirm').addClass('is-invalid');
                    $('#password-confirm-error').text(errors['password_confirmation'][0]);
                } else {
                    $('#password-confirm').removeClass('is-invalid');
                    $('#password-confirm-error').text('');
                }
            }
        });
    });
});
